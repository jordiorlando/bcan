EESchema Schematic File Version 2
LIBS:beacon-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:contrib
LIBS:valves
LIBS:bcan
LIBS:74xgxx
LIBS:ac-dc
LIBS:actel
LIBS:allegro
LIBS:Altera
LIBS:analog_devices
LIBS:atmel
LIBS:battery_management
LIBS:bbd
LIBS:bosch
LIBS:brooktre
LIBS:cmos_ieee
LIBS:dc-dc
LIBS:diode
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic_symbols
LIBS:hc11
LIBS:infineon
LIBS:intersil
LIBS:ir
LIBS:Lattice
LIBS:leds
LIBS:LEM
LIBS:logic_programmable
LIBS:maxim
LIBS:mechanical
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic24mcu
LIBS:microchip_pic32mcu
LIBS:modules
LIBS:motor_drivers
LIBS:motors
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:Oscillators
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:references
LIBS:relays
LIBS:rfcom
LIBS:RFSolutions
LIBS:sensors
LIBS:silabs
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:transf
LIBS:triac_thyristor
LIBS:ttl_ieee
LIBS:video
LIBS:wiznet
LIBS:Worldsemi
LIBS:Xicor
LIBS:zetex
LIBS:Zilog
LIBS:beacon-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "IR Beacon Board"
Date "2017-05-02"
Rev "2.0"
Comp "ECE 445"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L D_Bridge_-AA+ D3
U 1 1 58916D19
P 6200 4600
F 0 "D3" V 6450 4600 50  0000 R CNN
F 1 "BAS4002A" V 5950 4600 50  0000 R CNN
F 2 "TO_SOT_Packages_SMD:SOT-143" H 6200 4600 50  0001 C CNN
F 3 "" H 6200 4600 50  0000 C CNN
	1    6200 4600
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR01
U 1 1 58919FC5
P 7400 5000
F 0 "#PWR01" H 7400 4750 50  0001 C CNN
F 1 "GND" H 7400 4850 50  0000 C CNN
F 2 "" H 7400 5000 50  0000 C CNN
F 3 "" H 7400 5000 50  0000 C CNN
	1    7400 5000
	1    0    0    -1  
$EndComp
Text Notes 3650 3550 0    120  ~ 0
Control
Text Notes 5550 5250 0    120  ~ 0
Power
Text Notes 3650 6150 0    120  ~ 0
IR LED
Text Notes 5550 6150 0    120  ~ 0
Connectors
$Comp
L GND #PWR02
U 1 1 58C62B36
P 8600 5700
F 0 "#PWR02" H 8600 5450 50  0001 C CNN
F 1 "GND" H 8600 5550 50  0000 C CNN
F 2 "" H 8600 5700 50  0001 C CNN
F 3 "" H 8600 5700 50  0001 C CNN
	1    8600 5700
	1    0    0    -1  
$EndComp
Text Label 6600 4600 1    60   ~ 0
RAIL_R
Text Label 5800 4600 1    60   ~ 0
RAIL_L
$Comp
L VCC #PWR03
U 1 1 58D18C7E
P 6900 4100
F 0 "#PWR03" H 6900 3950 50  0001 C CNN
F 1 "VCC" H 6900 4250 50  0000 C CNN
F 2 "" H 6900 4100 50  0001 C CNN
F 3 "" H 6900 4100 50  0001 C CNN
	1    6900 4100
	1    0    0    -1  
$EndComp
$Comp
L C_Small C1
U 1 1 59017D8B
P 6900 4700
F 0 "C1" H 6910 4770 50  0000 L CNN
F 1 "10u" H 6910 4620 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 6900 4700 50  0001 C CNN
F 3 "" H 6900 4700 50  0001 C CNN
	1    6900 4700
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X01 J2
U 1 1 5901811B
P 5800 4000
F 0 "J2" H 5800 4100 50  0000 C CNN
F 1 "L" V 5900 4000 50  0000 C CNN
F 2 "bcan:Pad_Length10mm_Width3.5mm" H 5800 4000 50  0001 C CNN
F 3 "" H 5800 4000 50  0001 C CNN
	1    5800 4000
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X01 J1
U 1 1 5901817D
P 6600 4000
F 0 "J1" H 6600 4100 50  0000 C CNN
F 1 "R" V 6700 4000 50  0000 C CNN
F 2 "bcan:Pad_Length10mm_Width3.5mm" H 6600 4000 50  0001 C CNN
F 3 "" H 6600 4000 50  0001 C CNN
	1    6600 4000
	0    -1   -1   0   
$EndComp
$Comp
L R_Small R1
U 1 1 59018AF1
P 6800 2900
F 0 "R1" H 6830 2920 50  0000 L CNN
F 1 "10k" H 6830 2860 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" H 6800 2900 50  0001 C CNN
F 3 "" H 6800 2900 50  0001 C CNN
	1    6800 2900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 59029F1D
P 5200 3300
F 0 "#PWR04" H 5200 3050 50  0001 C CNN
F 1 "GND" H 5200 3150 50  0000 C CNN
F 2 "" H 5200 3300 50  0000 C CNN
F 3 "" H 5200 3300 50  0000 C CNN
	1    5200 3300
	1    0    0    -1  
$EndComp
$Comp
L R_Small R3
U 1 1 5902BB8D
P 4300 5100
F 0 "R3" H 4330 5120 50  0000 L CNN
F 1 "1M" H 4330 5060 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" H 4300 5100 50  0001 C CNN
F 3 "" H 4300 5100 50  0001 C CNN
	1    4300 5100
	1    0    0    -1  
$EndComp
$Comp
L R_Small R2
U 1 1 5902BC3D
P 4200 4700
F 0 "R2" H 4230 4720 50  0000 L CNN
F 1 "10" H 4230 4660 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" H 4200 4700 50  0001 C CNN
F 3 "" H 4200 4700 50  0001 C CNN
	1    4200 4700
	1    0    0    -1  
$EndComp
Text Label 4200 5500 1    60   ~ 0
RA1
$Comp
L R_Small R4
U 1 1 5A361BBA
P 4700 4700
F 0 "R4" H 4730 4720 50  0000 L CNN
F 1 "43K" H 4730 4660 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" H 4700 4700 50  0001 C CNN
F 3 "" H 4700 4700 50  0001 C CNN
	1    4700 4700
	1    0    0    -1  
$EndComp
$Comp
L R_Small R5
U 1 1 5A361C49
P 4800 5100
F 0 "R5" H 4830 5120 50  0000 L CNN
F 1 "13K" H 4830 5060 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" H 4800 5100 50  0001 C CNN
F 3 "" H 4800 5100 50  0001 C CNN
	1    4800 5100
	1    0    0    -1  
$EndComp
Text Label 4700 4300 3    60   ~ 0
RAIL_R
Text Label 4600 5000 0    60   ~ 0
RA2
$Comp
L GND #PWR05
U 1 1 5A361EA1
P 4700 5400
F 0 "#PWR05" H 4700 5150 50  0001 C CNN
F 1 "GND" H 4700 5250 50  0000 C CNN
F 2 "" H 4700 5400 50  0001 C CNN
F 3 "" H 4700 5400 50  0001 C CNN
	1    4700 5400
	1    0    0    -1  
$EndComp
$Comp
L TPS70950DBV U2
U 1 1 5A362228
P 7400 4400
F 0 "U2" H 7150 4650 60  0000 C CNN
F 1 "AP2204K-1.8" H 7600 4650 60  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23-5" H 7400 4400 60  0001 C CNN
F 3 "" H 7400 4400 60  0001 C CNN
	1    7400 4400
	1    0    0    -1  
$EndComp
$Comp
L LED_Small D1
U 1 1 5A3AA113
P 4100 5100
F 0 "D1" H 4050 5225 50  0000 L CNN
F 1 "940nm" H 3925 5000 50  0000 L CNN
F 2 "LEDs:LED_0603" V 4100 5100 50  0001 C CNN
F 3 "" V 4100 5100 50  0001 C CNN
	1    4100 5100
	0    -1   -1   0   
$EndComp
$Comp
L C_Small C2
U 1 1 5A3AC460
P 7900 4700
F 0 "C2" H 7910 4770 50  0000 L CNN
F 1 "1u" H 7910 4620 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 7900 4700 50  0001 C CNN
F 3 "" H 7900 4700 50  0001 C CNN
	1    7900 4700
	1    0    0    -1  
$EndComp
Text Label 4100 5000 0    60   ~ 0
LED
$Comp
L PIC10(L)F322-I/OT U1
U 1 1 5A511CEA
P 5900 3000
F 0 "U1" H 5450 3450 50  0000 L CNN
F 1 "PIC10LF322T-I/OT" H 5450 3350 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23-6" H 5450 2650 50  0001 L CIN
F 3 "" H 5900 3000 50  0001 C CNN
	1    5900 3000
	1    0    0    -1  
$EndComp
Text Label 6700 3100 2    60   ~ 0
~MCLR
Text Label 6700 2800 2    60   ~ 0
RA0
Text Label 6700 2900 2    60   ~ 0
RA1
Text Label 6700 3000 2    60   ~ 0
RA2
$Comp
L +1V8 #PWR06
U 1 1 5A513E7A
P 6800 2700
F 0 "#PWR06" H 6800 2550 50  0001 C CNN
F 1 "+1V8" H 6800 2840 50  0000 C CNN
F 2 "" H 6800 2700 50  0001 C CNN
F 3 "" H 6800 2700 50  0001 C CNN
	1    6800 2700
	1    0    0    -1  
$EndComp
$Comp
L +1V8 #PWR07
U 1 1 5A513F52
P 5200 2700
F 0 "#PWR07" H 5200 2550 50  0001 C CNN
F 1 "+1V8" H 5200 2840 50  0000 C CNN
F 2 "" H 5200 2700 50  0001 C CNN
F 3 "" H 5200 2700 50  0001 C CNN
	1    5200 2700
	1    0    0    -1  
$EndComp
$Comp
L +1V8 #PWR08
U 1 1 5A513F8A
P 7900 4100
F 0 "#PWR08" H 7900 3950 50  0001 C CNN
F 1 "+1V8" H 7900 4240 50  0000 C CNN
F 2 "" H 7900 4100 50  0001 C CNN
F 3 "" H 7900 4100 50  0001 C CNN
	1    7900 4100
	1    0    0    -1  
$EndComp
$Comp
L +1V8 #PWR09
U 1 1 5A513FC2
P 4200 4600
F 0 "#PWR09" H 4200 4450 50  0001 C CNN
F 1 "+1V8" H 4200 4740 50  0000 C CNN
F 2 "" H 4200 4600 50  0001 C CNN
F 3 "" H 4200 4600 50  0001 C CNN
	1    4200 4600
	1    0    0    -1  
$EndComp
$Comp
L +1V8 #PWR010
U 1 1 5A5141AF
P 8600 5600
F 0 "#PWR010" H 8600 5450 50  0001 C CNN
F 1 "+1V8" H 8600 5740 50  0000 C CNN
F 2 "" H 8600 5600 50  0001 C CNN
F 3 "" H 8600 5600 50  0001 C CNN
	1    8600 5600
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x06 J3
U 1 1 5A5142E0
P 9200 5700
F 0 "J3" H 9200 6000 50  0000 C CNN
F 1 "ICSP" H 9200 5300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm_SMD_Pin1Left" H 9200 5700 50  0001 C CNN
F 3 "" H 9200 5700 50  0001 C CNN
	1    9200 5700
	1    0    0    -1  
$EndComp
Text Label 8800 5500 0    60   ~ 0
~MCLR
Wire Notes Line
	5500 6200 5500 5500
Wire Notes Line
	8200 6200 5500 6200
Wire Notes Line
	8200 5500 8200 6200
Wire Notes Line
	5500 5500 8200 5500
Connection ~ 4200 4900
Wire Wire Line
	4200 4900 4200 4800
Wire Wire Line
	4100 4900 4300 4900
Wire Wire Line
	4100 5000 4100 4900
Wire Wire Line
	4100 5200 4100 5300
Wire Wire Line
	7400 4700 7400 5000
Connection ~ 7400 4900
Wire Notes Line
	3600 6200 5300 6200
Wire Notes Line
	3600 3600 3600 2400
Wire Notes Line
	8200 2400 8200 3600
Wire Notes Line
	5500 5300 5500 3800
Wire Notes Line
	5500 3800 8200 3800
Wire Notes Line
	8200 3800 8200 5300
Wire Notes Line
	8200 5300 5500 5300
Wire Notes Line
	5300 6200 5300 3800
Wire Notes Line
	5300 3800 3600 3800
Wire Notes Line
	3600 3800 3600 6200
Wire Notes Line
	3600 2400 8200 2400
Wire Notes Line
	8200 3600 3600 3600
Wire Wire Line
	6200 4300 7000 4300
Wire Wire Line
	6600 4200 6600 4600
Wire Wire Line
	6600 4600 6500 4600
Wire Wire Line
	5800 4200 5800 4600
Wire Wire Line
	5800 4600 5900 4600
Connection ~ 6900 4300
Wire Wire Line
	4100 5300 4300 5300
Wire Wire Line
	4200 5300 4200 5500
Connection ~ 4200 5300
Wire Wire Line
	4700 4600 4700 4300
Wire Wire Line
	4300 5300 4300 5200
Wire Wire Line
	4300 4900 4300 5000
Wire Wire Line
	6900 4100 6900 4600
Wire Wire Line
	5300 2800 5200 2800
Wire Wire Line
	5300 3200 5200 3200
Wire Wire Line
	7800 4300 7900 4300
Wire Wire Line
	7900 4100 7900 4600
Wire Wire Line
	7900 4900 7900 4800
Wire Wire Line
	6200 4900 7900 4900
Connection ~ 7900 4300
Wire Wire Line
	6900 4800 6900 4900
Connection ~ 6900 4900
Wire Wire Line
	7000 4500 6900 4500
Connection ~ 6900 4500
Wire Wire Line
	6500 3100 6800 3100
Wire Wire Line
	6800 3100 6800 3000
Wire Wire Line
	6500 3000 6700 3000
Wire Wire Line
	6500 2900 6700 2900
Wire Wire Line
	6500 2800 6700 2800
Wire Wire Line
	9000 5500 8800 5500
Wire Wire Line
	9000 5800 8800 5800
Wire Wire Line
	9000 5900 8800 5900
Wire Wire Line
	9000 6000 8800 6000
Wire Wire Line
	9000 5600 8600 5600
Wire Wire Line
	9000 5700 8600 5700
Text Label 8800 5800 0    60   ~ 0
RA0
Text Label 8800 5900 0    60   ~ 0
RA1
Text Label 8800 6000 0    60   ~ 0
RA2
Wire Wire Line
	6800 2800 6800 2700
$Comp
L D_Zener_Small D2
U 1 1 5A515ABB
P 4600 5100
F 0 "D2" H 4600 5190 50  0000 C CNN
F 1 "3V3" H 4600 5010 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-523" V 4600 5100 50  0001 C CNN
F 3 "" V 4600 5100 50  0001 C CNN
	1    4600 5100
	0    1    1    0   
$EndComp
Wire Wire Line
	4600 5000 4600 4900
Wire Wire Line
	4600 4900 4800 4900
Wire Wire Line
	4700 4900 4700 4800
Wire Wire Line
	4800 4900 4800 5000
Connection ~ 4700 4900
Wire Wire Line
	4600 5200 4600 5300
Wire Wire Line
	4600 5300 4800 5300
Wire Wire Line
	4700 5300 4700 5400
Wire Wire Line
	4800 5300 4800 5200
Connection ~ 4700 5300
Wire Wire Line
	5200 2800 5200 2700
Wire Wire Line
	5200 3200 5200 3300
$EndSCHEMATC
